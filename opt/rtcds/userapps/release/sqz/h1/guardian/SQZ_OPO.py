# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_OPO.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator, NodeManager

nominal = 'LOCKED_CLF_DUAL'

#############################################
#Functions

def OPO_locked():
    opo_trans_norm = 0.5
    return ezca['SQZ-OPO_TRANS_DC_NORMALIZED'] > opo_trans_norm

def in_softfault():
    if ezca['SQZ-OPO_PZT_1_RANGE'] != 0:
        notify('PZT voltage limits exceeded.')
        log('PZT voltage limits exceeded.')
        return True
    elif not OPO_locked():
        notify('OPO not really locked or maybe locked in a wrong mode')
        log('OPO not really locked or maybe locked in a wrong mode')
        return True


def in_hardfault():
    if ezca['SQZ-LASER_IR_DC_ERROR_FLAG'] != 0:
        notify('Squeezer laser PD error')
        log('Squeezer laser PD error')
        return True

def CLOSE_INTENSITY_STAB_SERVO():
    #close intensity stabilization loop
    ezca['SQZ-OPO_SERVO_IN1GAIN'] = 6
    ezca['SQZ-OPO_SERVO_IN2GAIN'] = 6
    ezca['SQZ-OPO_SERVO_IN1POL'] = 1
    ezca['SQZ-OPO_SERVO_IN2POL'] = 0
    ezca['SQZ-OPO_SERVO_IN1EN'] = 1
    ezca['SQZ-OPO_SERVO_IN2EN'] = 1
    ezca['SQZ-OPO_SERVO_FASTEN'] = 1
    #The only way I know how to ramp slidebar gain
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = -31
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = -21
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = -11
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = -1
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = 11
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = 21
    ezca['SQZ-OPO_SERVO_FASTGAIN'] = 31

'''
def CLF_shutter_check():
    if ezca['SYS-MOTION_C_SHUTTER_H_STATE'] == 1:
        #notify('CLF shutter closed. Check fiber switch before open. CLF should go in.')
        #log('CLF shutter closed. Check fiber switch before open. CLF should go in.') 
        #Just send a message and continue. User should check whether if seed or CLF is being used (NK Aug28).
        return False
    else:
        return True 
'''

def EOM_OK():
    return abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4 

def LO_NOT_RAILING():
    log('Checking LO in LO_NOT_RAILING()')
    flag = False
    if abs(ezca['SQZ-LO_SERVO_FASTMON']) < 10 or abs(ezca['SQZ-LO_SERVO_SLOWMON']) < 10:
       flag = True
    return flag
        

#def Power2high():
#    if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] > 30:
#        return True
        


#############################################
#Decorator


class softfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_softfault():
            return 'DOWN'

class hardfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_hardfault():
            return 'IDLE'

class EOM_checker(GuardStateDecorator):
    def pre_exec(self):
        if not EOM_OK():
            ezca['SQZ-FIBR_SERVO_EOMEN']=0
            return 'CHECK_EOM'
            
'''
class CLF_shutter_check(GuardStateDecorator):
    def pre_exc(sefl):
        if CLF_shutter_check() == False:
            notify('CLF shutter closed. Check fiber switch before open. CLF should go in.')
            log('CLF shutter closed. Check fiber switch before open. CLF should go in.') 
            return 'SCANNING_CLF_DUAL'
'''

#class Power2high(GuardStateDecorator):
#    def pre_exc(self):
#        if Power2high:
#           ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 0
#           notify('Pump input power > 23mW. Flipper down.')
#           return 'IDLE'
        
#############################################        
#nodes

#nodes = NodeManager(['SQZ_PLL','SQZ_CLF'])


#############################################        
#States

class INIT(GuardState):
    index = 0

    def main(self):
        return True

#        if OPO_locked():
#           return 'LOCKED'
#        else:
#           return 'DOWN'

class DOWN(GuardState):
    index = 1
    goto = True

    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-FIBR_SERVO_BOOST1EN']=0
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ERRORSIGNAL'] = 1 #change error signal to beatnote
        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0 #turn off boost1
        ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0 #turn off boost2
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] = 0
        ezca['SQZ-OPO_PZT_1_OFFSET'] = 10 #Need to leave room for beatnote optimization
        #ezca['SQZ-LASER_HEAD_CRYSTALFREQUENCY'] = 50
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_POLARITY']=1
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=1 #turn on laser temp feedback
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=0 #close the flipper
        ezca['SQZ-FIBR_SERVO_FASTPOL']=1 #PZT pol

        
        #open intensity stabilization loop (NK Dec17)
        ezca['SQZ-OPO_SERVO_IN1POL'] = 1
        ezca['SQZ-OPO_SERVO_IN2POL'] = 0
        ezca['SQZ-OPO_SERVO_FASTPOL'] = 1
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0
        ezca['SQZ-OPO_SERVO_IN2EN'] = 0
        ezca['SQZ-OPO_SERVO_FASTEN'] = 0
        ezca['SQZ-OPO_SERVO_FASTGAIN'] = -32
        ezca['SQZ-OPO_SERVO_COMCOMP'] = 1
        ezca['SQZ-OPO_SERVO_COMFILTER'] = 1
   
        #Tell OPO common mode screen to unlock (PLL), tell frequency correction to stop feeding back (FREQ), tell CLF to go DOWN
        #Took out FREQ since we don't use it anymore, comment out LO just because it's not ready (NK Dec 17)
        #nodes['SQZ_PLL'] = 'DOWN'
        #nodes['SQZ_CLF'] = 'DOWN'
        #nodes['SQZ_LO'] = 'DOWN'

        
    #@Power2high
    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False
    #goto = True

    def run(self):
        notify('Cannot lock OPO. Check pump light on SQZT6.')
        log('Cannot lock OPO. Check pump light on SQZT6')
        return (ezca['SQZ-SHG_GR_DC_NORMALIZED'] > 0.3) and (ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] < 30) #20mW to protect to fiber
   

####################################################
# CLF LOCKING
####################################################
class PREP_LOCK_CLF(GuardState):
    index = 14
    request = False
    
    def main(self):
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1 
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 1
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0
        notify('CLF light in.')

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 34 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 33:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 33.33

    def run(self):
        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 34 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 33:
            return True



class SCANNING_CLF_DUAL(GuardState):
    index = 15
    request = False


    def main(self):
        if ezca['SYS-MOTION_C_SHUTTER_H_STATE'] == 1:
           notify('CLF shutter closed. Check fiber switch before open. CLF should go in.')
           log('CLF shutter closed. Check fiber switch before open. CLF should go in.') 
           return 'SCANNING_CLF_DUAL'
            #Just send a message and continue. User should check whether if seed or CLF is being used (NK Aug28).

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0 #turn off boost1
        ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0 #turn off boost2
        #ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=2 # Set PZT scan trigger to 'And'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 0 # This looks at refl trans normalized AND 6MHz RF mon
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.6
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 0.5
      
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False



def LOCKING():
    class LOCKING(GuardState):
        request = False

        def main(self):
            ezca['SQZ-FIBR_SERVO_RAMPEN']=0 #engage OPO locking
            time.sleep(1)

        def run(self):
            if ezca['SQZ-OPO_PZT_1_RANGE'] != 0: #if not okay
                notify('PZT out of range. Try again.')
                log('PZT out of range. Try again.')
                return 'IDLE'

            #check if OPO is locked fine. Then engage EOM, boosts
            if OPO_locked():   
                ezca['SQZ-FIBR_SERVO_EOMEN'] = 1
                ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 1
                ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 1
                time.sleep(0.5)

                return True
            else:
                return 'IDLE'
    return LOCKING



LOCKING_CLF_DUAL = LOCKING()
LOCKING_CLF_DUAL.index = 16



class LOCKED_CLF_DUAL(GuardState):
    index = 17
    
    @softfault_checker
    @hardfault_checker
    
    def main(self):     
        if OPO_locked():
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_POLARITY']=0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ERRORSIGNAL'] = 0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1

            CLOSE_INTENSITY_STAB_SERVO()

        return True

    @softfault_checker
    @hardfault_checker
    @EOM_checker

    def run(self):

        return True



class CHECK_EOM(GuardState):
    index = 18
    goto = False #So that Guardian won't find the shortest path to LOCKED with this state
    request = False

    @softfault_checker #this will make OPO rescan if it's not locked
    def run(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1 #close beam diverter
        notify('EOM noisy or railed, disengaged.')
        log('EOM noisy or railed, disengaged.')

        if LO_NOT_RAILING():
            ezca['SQZ-FIBR_SERVO_EOMEN'] = 1

        if (ezca['SQZ-FIBR_SERVO_EOMEN'] == 1) and EOM_OK(): #If manually engage EOM and it doesn't rail
            return 'LOCKED_CLF_DUAL'

        #if OPO_locked() and (ezca['PSL-FSS_TPD_DC_OUTPUT']>1.5): #If OPO locks and ref cav OK
        #    return 'LOCKED_ON_DUAL'
        #if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] >20:
        #    return 'IDLE'




    
####################################################
# SEED HIGH POWER LOCKING - For IFO Alignment
####################################################
class PREP_LOCK_SEED_HIGH(GuardState):
    index = 24
    request = False
    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 50:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 51.1

    def run(self):
        # Set SEED Power to ~>40mW    
        # changed to 60 for Dan's TCS work (Dec 6, 2018 NK)    
        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] < 57:
            self.counter = 0
            if self.counter == 0:
                ezca['SYS-MOTION_C_PICO_I_SELECTEDMOTOR'] = 6
                ezca['SYS-MOTION_C_PICO_I_CURRENT_STEPSIZE'] = 100
                ezca['SYS-MOTION_C_PICO_I_CURRENT_DRIVEDELAY'] = 4 
                self.counter += 1
            if ezca['SYS-MOTION_C_PICO_I_INUSE'] == 0:
                ezca['SYS-MOTION_C_PICO_I_MOTOR_6_DRIVE'] = 4 # DOWN =3 and UP = 4

        else:
            if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 50 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 53:
                notify('High power SEED light in.')
                return True



class SCANNING_SEED_HIGH(GuardState):
    index = 25
    request = False


    def main(self):

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0 #turn off boost1
        ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0 #turn off boost2
        #ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD'] = 100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT'] = 2 #Set PZT scan trigger to 'Ch1 & Ch2'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 1 # This looks at refl trans and OPO IR.
        
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.5
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 5 # SQZ-OPO_IR_DC_POWER channel
        
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False




LOCKING_SEED_HIGH = LOCKING()
LOCKING_SEED_HIGH.index = 26



class LOCKED_SEED_HIGH(GuardState):
    index = 27

    @softfault_checker
    @hardfault_checker
    
    def main(self):
        if OPO_locked():
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_POLARITY']=0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ERRORSIGNAL'] = 0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1
            CLOSE_INTENSITY_STAB_SERVO()

            if ezca['SQZ-OPO_IR_RESONANCE_SEED_NORM'] > 0.8:
                notify('OPO locked with high SEED.')
                return True
        


    
####################################################
# SEED LOW POWER LOCKING - For NLG Measurement
####################################################
class PREP_LOCK_SEED_LOW(GuardState):
    index = 34
    request = False
    
    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 34 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 33:
            ezca['SQZ-OPO_TEC_SETTEMP'] = 33.33

    def run(self):
        # Set SEED Power to ~<1mW       
        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 1 or ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] < 0.5:
            self.counter = 0
            if self.counter == 0:
                ezca['SYS-MOTION_C_PICO_I_SELECTEDMOTOR'] = 6
                ezca['SYS-MOTION_C_PICO_I_CURRENT_STEPSIZE'] = 100
                ezca['SYS-MOTION_C_PICO_I_CURRENT_DRIVEDELAY'] = 4 
                self.counter += 1
            if ezca['SYS-MOTION_C_PICO_I_INUSE'] == 0:
                ezca['SYS-MOTION_C_PICO_I_MOTOR_6_DRIVE'] = 3 # DOWN =3 and UP = 4
            
        else:
            if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 33 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 34:
                notify('Low power SEED light in.')
                return True
                
            

class SCANNING_SEED_LOW(GuardState):
    index = 35
    request = False


    def main(self):

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_SERVO_BOOST2EN'] = 0 #turn off boost1
        ezca['SQZ-FIBR_SERVO_BOOST3EN'] = 0 #turn off boost2
        #ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD'] = 100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT'] = 2 # Set PZT scan trigger to 'Ch1 & Ch2'. 
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 1 # This looks at refl trans and OPO IR.

        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.5
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 0.05 # SQZ-OPO_IR_DC_POWER channel
        
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False




LOCKING_SEED_LOW = LOCKING()
LOCKING_SEED_LOW.index = 36



class LOCKED_SEED_LOW(GuardState):
    index = 37

    @softfault_checker
    @hardfault_checker
    
    def main(self):
        if OPO_locked():
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_POLARITY']=0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ERRORSIGNAL'] = 0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1
            CLOSE_INTENSITY_STAB_SERVO()

            if ezca['SQZ-OPO_IR_RESONANCE_SEED_NORM'] > 0.1:
                notify('OPO locked with low SEED, please measure NLG.')
                return True




            


################################################################
  
LOCKING_ON_ANY = LOCKING()
LOCKING_ON_ANY.index = 6
    
class SCANNING(GuardState):
    index = 4
    request = False
    
    def main(self):
        ezca['SQZ-OPO_PZT_1_OFFSET'] = 0
        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM
        ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=100 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=0 #trigger on green trans (NK Aug 28)
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.8

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False





class LOCKED(GuardState):
    index = 10

    @softfault_checker
    @hardfault_checker
    def main(self):
        if OPO_locked():
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_POLARITY']=0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ERRORSIGNAL'] = 0
            ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON'] = 1
            #close intensity stabilization loop
            ezca['SQZ-OPO_SERVO_IN1GAIN'] = 6
            ezca['SQZ-OPO_SERVO_IN2GAIN'] = 6
            ezca['SQZ-OPO_SERVO_IN1POL'] = 1
            ezca['SQZ-OPO_SERVO_IN2POL'] = 0
            ezca['SQZ-OPO_SERVO_IN1EN'] = 1
            ezca['SQZ-OPO_SERVO_IN2EN'] = 1
            ezca['SQZ-OPO_SERVO_FASTEN'] = 1
            #The only way I know how to ramp slidebar gain
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = -31
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = -21
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = -11
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = -1
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = 11
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = 21
            ezca['SQZ-OPO_SERVO_FASTGAIN'] = 31
            #lock CLF
            #nodes['SQZ_CLF'] = 'LOCKED'
        return True

    @softfault_checker
    @hardfault_checker
    @EOM_checker

    def run(self):
        #if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
        #    if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
        #        notify('Scan timeout. Check trigger level.')
        #        log('Scan timeout')
        #        return 'IDLE'
        #    return True

        #if OPO_locked():
        #    return True
        #else:
        #    return 'IDLE' 

        return True




##################################################

edges = [
    ('INIT', 'DOWN'),
    #('DOWN', 'IDLE'),
    
    # LOCK CLF
    ('DOWN', 'PREP_LOCK_CLF'),
    ('PREP_LOCK_CLF', 'SCANNING_CLF_DUAL'),
    ('SCANNING_CLF_DUAL', 'LOCKING_CLF_DUAL'),
    ('LOCKING_CLF_DUAL', 'LOCKED_CLF_DUAL'),
    ('IDLE', 'SCANNING_CLF_DUAL'),
    ('CHECK_EOM', 'LOCKED_CLF_DUAL'),
    
    # LOCK SEED HIGH
    ('DOWN', 'PREP_LOCK_SEED_HIGH'),
    ('PREP_LOCK_SEED_HIGH', 'SCANNING_SEED_HIGH'),
    ('SCANNING_SEED_HIGH', 'LOCKING_SEED_HIGH'),
    ('LOCKING_SEED_HIGH', 'LOCKED_SEED_HIGH'),
    ('IDLE', 'SCANNING_SEED_HIGH'),

    
    # LOCK SEED LOW
    ('DOWN', 'PREP_LOCK_SEED_LOW'),
    ('PREP_LOCK_SEED_LOW', 'SCANNING_SEED_LOW'),
    ('SCANNING_SEED_LOW', 'LOCKING_SEED_LOW'),
    ('LOCKING_SEED_LOW', 'LOCKED_SEED_LOW') ,
    ('IDLE', 'SCANNING_SEED_LOW'),
    


    # LOCK ANY
    ('DOWN', 'SCANNING'),
    ('LOCKED','DOWN'),
    ('IDLE', 'SCANNING'),
    ('SCANNING', 'LOCKING_ON_ANY'),
    ('LOCKING_ON_ANY', 'LOCKED')]
    
